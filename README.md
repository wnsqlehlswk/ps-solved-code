# online judge

## [beakjoon online judge](https://www.acmicpc.net/)

![alt](./image/boj.png)

to see user page [here](https://www.acmicpc.net/user/wnsqlehlswk)

## [codeforce](http://codeforces.com)

user page [here](https://codeforces.com/profile/demetoir)

## [hacker earth](https://www.hackerearth.com/challenges/)

user page [here](https://www.hackerearth.com/@wnsqlehlswk)

# competition history

* 2016 lg code monster 본선 진출
* 2016 scpc 예선 2차 진출
* 2017 scpc 예선 2차 진출

## used language 

* c++
* python3
* pypy(python2.7)
